#pragma once
#include <string>

#include "IOsoba.h"

class Magister : public IOsoba
{
	private:
		string _imie;
		string _nazwisko;

	public:
		Magister(string, string);
		~Magister(void);

		string IOsoba::ImieNazwisko();
};


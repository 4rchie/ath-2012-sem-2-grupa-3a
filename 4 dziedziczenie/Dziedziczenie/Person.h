#pragma once
#include <string>

using namespace std;

class Person
{
protected:
	string name;
	string surname;

public:
	Person();
	~Person(void);

	void SetName(string);
	void SetSurname(string);
	void WhoAreYou();
	void SayHello();
};


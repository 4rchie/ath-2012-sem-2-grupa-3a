#pragma once
#include <string>
using namespace std;

class Student
{
	private:
		string _name;
		string _surname;

	public:
		Student(string name, string surname);
		~Student(void);

		string Name();
		string Surname();
};


#pragma once
#include "IOsoba.h"

class Doktor : public IOsoba
{
	private:
		string _imie;
		string _nazwisko;
	public:
		Doktor(string,string);
		~Doktor(void);

		string IOsoba::ImieNazwisko();
};


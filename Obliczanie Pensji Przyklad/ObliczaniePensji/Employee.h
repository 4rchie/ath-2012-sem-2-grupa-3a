#pragma once

#include <string>
#include <vector>
#include "Bonus.h"

using namespace std;

class Employee
{
public:
	Employee(void);
	~Employee(void);

	void SetName(string name);
	string Name();

	void SetSurname(string surname);
	string Surname();

	void SetBaseSalary(int baseSalary);
	float  BaseSalary();

	vector<Bonus*>* Bonuses();

private:
	string _name;
	string _surname;
	float _baseSalary;
	vector<Bonus*>* _bonuses;
};



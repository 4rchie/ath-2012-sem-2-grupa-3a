#pragma once
#include "bonus.h"

class DemoBonus : public Bonus
{
public:
	DemoBonus(void);
	~DemoBonus(void);
	string Bonus::Name();
	void Bonus::SetValue(int value);
	int Bonus::Value();
	float Bonus::Tax(); 

private:
	int _value;
};


Temat: 
  Maszyna sprzedaj�ca kanapki
 
  Automat wy�wietla list� dost�pnych kanapek wraz z ich cenami i sk�adem - ka�da kanapka mo�e mie� inn� cen�. 
  U�ytkownik jest proszony o wrzucenie odpowiedniej kwoty w monetach a nast�pnie wybranie kanapki. 
  Je�eli u�ytkownik wrzuci� tak� sam� lub wi�ksz� kwot�, w�wczas automat wydaje kanapk� oraz reszt�. 
 
Za�o�enia:
 
  1) automat obs�uguje monety 1, 2, 5 z� oraz 10, 20, 50 gr
  
  2) ceny kanapek s� zaokr�glane do 0.10 gr czyli mo�liwe ceny kanapek to 1.20, 2.50, 1.80
  nieprawid�owe ceny to 1.01, 0.59, 2.11 itd.
  
  3) lista kanapek musi by� tak skonstruowana aby �atwo by�o dodawa�/usuwa� nowe

  4) sk�ad kanapek mo�e by� przechowywana jako string np.: "mas�o, sa�ata, jajako, szynka"
  
  4) u�ytkownik musi mie� mo�liwo�� anulowania zam�wienia
  
  5) aplikacja musi mie� minimum 2 dost�pne kanapki (chocia� mo�e by� ich wi�cej, np 5)
  
 
 Za�o�enia do kolokwium:
 
  1) mo�na korzysta� z Internetu, ksi��ek i notatek
  
  2) nie mo�na: rozmawia�, ogl�da� cudzy kod czy komunikowa� si� w jakikolwiek spos�b pod kar� natychmiastowego wyproszenia z sali
  
  3) kod musi zosta� zwr�cony do repozytorium. Po up�yni�ciu czasu repozytorium zostanie zablokowane tylko do odczytu dlatego czas prosz� pilnowa� czasu.
  
  4) kod musi si� kompilowa� i uruchamia� - TYLKO TAKI KOD b�dzie sprawdzany
#pragma once
#include <string>

using namespace std;

class Bonus
{
public:
	virtual string Name() = 0;
	virtual int Value() = 0;
	virtual void SetValue(int value) = 0;
	virtual float Tax() = 0; 
};


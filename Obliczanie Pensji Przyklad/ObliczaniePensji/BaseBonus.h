#pragma once
#include "bonus.h"

class BaseBonus : public Bonus
{
public:
	BaseBonus(void);
	~BaseBonus(void);

	string Bonus::Name();
	void Bonus::SetValue(int value);
	int Bonus::Value();
	float Bonus::Tax(); 

private:
	int _value;
};


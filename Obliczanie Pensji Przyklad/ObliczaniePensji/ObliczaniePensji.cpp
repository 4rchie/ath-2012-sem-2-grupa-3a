// ObliczaniePensji.cpp : Defines the entry point for the console application.
//
#include <string>
#include <iostream>
#include "stdafx.h"

#include "Factory.h"

using namespace std;

void NewLine(){
	cout << endl;
}

void FillBaseData(Employee* employee){
	NewLine();
	string name;
	string surname;
	int baseSalary;

	cout << "podaj imie\t: "; cin >> name; 
	cout << "podaj nazwisko\t: "; cin >> surname;
	cout << "podaj podstawe\t: "; cin >> baseSalary;
	
	employee->SetName(name);
	employee->SetSurname(surname);
	employee->SetBaseSalary(baseSalary);
}

void FillAdditions(Employee* employee){

	auto bonuses = employee->Bonuses();
	for(int i = 0; i < bonuses->size(); i++){
		auto bonus = bonuses->at(i);
		int value;
		cout << bonus->Name() << "\t: ";
		cin >> value;
		bonus->SetValue(value);
	}
}

void FillEmplyeeDetails(Employee* employee){

	FillBaseData(employee);
	FillAdditions(employee);
}

void ShowBaseData(Employee* employee){
	cout << employee->Name() << " " << employee->Surname(); NewLine();
	cout << "podstawa : " << employee->BaseSalary(); NewLine();
}

void ShowBonuses(Employee* employee){
	auto bonuses = employee->Bonuses();

	for(int i = 0; i < bonuses->size(); i++){
		auto bonus = bonuses->at(i);
		cout << bonus->Name() << "\t: " << bonus->Value() << "\t" << bonus->Tax() * 100 << "%";
		NewLine();
	}
}

void ShowEmployee(Employee* employee){
	ShowBaseData(employee);
	ShowBonuses(employee);
}


int main(){

	auto factory = new Factory();
	auto emplyee = factory->CreateEmployee();
	auto salaryCalculator = factory->CreateSalaryCalculator();
	
	FillEmplyeeDetails(emplyee);
	ShowEmployee(emplyee);

	NewLine();
	NewLine();

	cout << "pensja : " << salaryCalculator->Calculate(emplyee);

	NewLine();

	system("pause");
}
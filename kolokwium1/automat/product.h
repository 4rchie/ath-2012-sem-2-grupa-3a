#pragma once
#include <string>
#include <vector>

using namespace std;

class product
{
public:
	virtual string name() = 0;
	virtual string ingredients() = 0;
	virtual int price() = 0;
};

#include "SalaryCalculator.h"


SalaryCalculator::SalaryCalculator(void)
{
}


SalaryCalculator::~SalaryCalculator(void)
{
}

float SalaryCalculator::Calculate(Employee* emplyee){
	float salary = 0;
	salary = salary + emplyee->BaseSalary();
	
	auto bonuses = emplyee->Bonuses();
	for(int i = 0; i < bonuses->size(); i++){
		auto bonus = bonuses->at(i);
		salary = salary + bonus->Value();
	}

	return salary;
}
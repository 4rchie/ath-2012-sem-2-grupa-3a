#include "Factory.h"
#include "BaseBonus.h"
#include "MotivationBonus.h"
#include "DemoBonus.h"

Factory::Factory(void)
{
}


Factory::~Factory(void)
{
}

Employee* Factory::CreateEmployee(){

	auto newEmployee = new Employee();

	newEmployee->Bonuses()->push_back(new BaseBonus());
	newEmployee->Bonuses()->push_back(new BaseBonus());
	newEmployee->Bonuses()->push_back(new BaseBonus());
	newEmployee->Bonuses()->push_back(new BaseBonus());
	newEmployee->Bonuses()->push_back(new MotivationBonus());
	newEmployee->Bonuses()->push_back(new MotivationBonus());
	newEmployee->Bonuses()->push_back(new MotivationBonus());
	newEmployee->Bonuses()->push_back(new MotivationBonus());
	newEmployee->Bonuses()->push_back(new MotivationBonus());
	newEmployee->Bonuses()->push_back(new DemoBonus());
	newEmployee->Bonuses()->push_back(new DemoBonus());
	newEmployee->Bonuses()->push_back(new DemoBonus());
	newEmployee->Bonuses()->push_back(new DemoBonus());
	newEmployee->Bonuses()->push_back(new DemoBonus());
	newEmployee->Bonuses()->push_back(new DemoBonus());
	newEmployee->Bonuses()->push_back(new DemoBonus());

	return newEmployee;
}

SalaryCalculator* Factory::CreateSalaryCalculator(){
	return new SalaryCalculator();
}


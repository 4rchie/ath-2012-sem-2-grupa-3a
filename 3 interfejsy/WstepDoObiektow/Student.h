#pragma once
#include <string>

#include "IOsoba.h"

using namespace std;


class Student : public IOsoba
{
	private:
		string _imie;
		string _nazwisko;

	public:
		Student(string, string);
		~Student(void);

		string IOsoba::ImieNazwisko();
};


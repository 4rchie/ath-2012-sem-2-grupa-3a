#include <string>
#include "Magister.h"


Magister::Magister(string imie, string nazwisko)
{
	_imie = imie;
	_nazwisko = nazwisko;
}

string Magister::ImieNazwisko(){
	return "mgr " + Magister::_imie + " " + Magister::_nazwisko;
}

Magister::~Magister(void)
{
}
